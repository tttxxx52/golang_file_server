package config

import (
	"github.com/gin-gonic/gin"
	"os"
)

type configServer struct {
	ListenPort string
	Mode string
	Host string
}

var ServerConfig configServer

func initServerConfig() {

	switch gin.Mode() {
	case gin.ReleaseMode:
		ServerConfig = configServer{"10000","release", ""}
	case gin.DebugMode:
		ServerConfig = configServer{os.Getenv("LISTEN_PORT"), os.Getenv("MODE"), os.Getenv("HOST")}
	case gin.TestMode:
		ServerConfig = configServer{"10000","release", ""}
	}
}