module file_server

go 1.12

require (
	github.com/dgryski/dgoogauth v0.0.0-20190221195224-5a805980a5f3
	github.com/disintegration/imaging v1.6.2
	github.com/garyburd/redigo v1.6.0
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-gonic/gin v1.5.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/joho/godotenv v1.3.0
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/pkg/errors v0.9.1
	github.com/zeekay/gochimp3 v0.0.0-20191219204354-bad654ab6826
	go.mongodb.org/mongo-driver v1.3.3
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5
	golang.org/x/text v0.3.2
)
