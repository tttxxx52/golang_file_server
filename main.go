package main

import (
	"file_server/config"
	"file_server/database/mysql"
	"file_server/handler"
	"file_server/service"
	"file_server/util/log"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"os"
)

func init() {
	if err := godotenv.Load(".env"); err != nil {
		gin.SetMode(gin.ReleaseMode)
	} else {
		gin.SetMode(os.Getenv("MODE"))
	}

	// 初始化設定
	config.InitConfig()
}


func main() {
	//建立新專案時使用
	//if execCommand() {
	//	return
	//}

	//DataBase Pool
	mysql.OpenConnect()


	router := gin.Default()
	router.Use(cors.New(cors.Config{
		AllowAllOrigins: true,
		AllowMethods: []string{"GET","POST"},
		AllowHeaders: []string{"Content-Type","Access-Control-Allow-Origin","Auth-Token","drive-type"},
	}))

	router.POST("/token-precede",handler.TokenPrecede)
	router.Use(service.AuthToken())
	{
		router.POST("/file-server-api", handler.Upload)
		router.POST("/file-server-base-api", handler.UploadBase64)

	}

	//_ = router.Run(":" + config.ServerConfig.ListenPort)
	log.Error(router.Run(":10000"))
}

//func execCommand() bool {
//
//			var projectKey model.ProjectKey
//			project := "rc_wisdom_helper"
//			secret := crypto.GenerateRandomCode(10)
//			projectKey.SetProject(project).
//				SetApiKey(crypto.Md5(project + secret)).
//				SetSecret(secret).
//				SetCreatedAt(time.Now()).
//				Create()
//
//	return  true
//}