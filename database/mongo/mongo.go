package mongo

import (

	"context"
	"file_server/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type MongoHandler struct {
	client *mongo.Client
}

func (mongoHandler *MongoHandler) NewConnect() *MongoHandler {
	credential := options.Credential{
		Username: config.MongoConfig.Username,
		Password: config.MongoConfig.Password,
	}

	clientOptions := options.Client().ApplyURI("mongodb://" + config.MongoConfig.Host).SetAuth(credential)
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	err = client.Connect(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	mongoHandler.client = client

	return mongoHandler
}

func NewMongo(option func(*MongoHandler)) {
	mongoHandler := &MongoHandler{}
	client := mongoHandler.NewConnect()
	option(client)
	defer mongoHandler.client.Disconnect(context.Background())
}

func (mongoHandler *MongoHandler) Collection(collectionName string) *mongo.Collection {
	return mongoHandler.client.Database(config.MongoConfig.DBName).Collection(collectionName)
}
