package redis

import (
	"encoding/json"
	"errors"
	"file_server/config"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"reflect"
	"time"
)


// NewRedisPool 返回redis连接池
func NewRedisPool() *redis.Pool {
	return &redis.Pool{
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.DialURL(config.RedisConfig.Host+":"+config.RedisConfig.Host)
			if err != nil {
				return nil, fmt.Errorf("redis connection error: %s", err)
			}
			//验证redis密码
			if _, authErr := c.Do("AUTH", config.RedisConfig.Password); authErr != nil {
				return nil, fmt.Errorf("redis auth password error: %s", authErr)
			}

			if _, dbErr := c.Do("SELECT", config.RedisConfig.DBName); dbErr != nil {
				return nil, fmt.Errorf("redis auth password error: %s", dbErr)
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			if err != nil {
				return fmt.Errorf("ping redis error: %s", err)
			}
			return nil
		},
	}
}

////////////////////////////
///       key-value      ///
////////////////////////////

func Set(key, value string) {
	c := NewRedisPool().Get()
	defer c.Close()
	_, err := c.Do("SET", key, value)
	if err != nil {
		fmt.Println("set error", err.Error())
	}
}

func Get(key string) string {
	c := NewRedisPool().Get()
	defer c.Close()
	value, err := redis.String(c.Do("GET", key))
	if err != nil {
		fmt.Println("Get Error: ", err.Error())
		return ""
	}
	return value
}

func DelKey(key string) error {
	c := NewRedisPool().Get()
	defer c.Close()
	_, err := c.Do("DEL", key)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func IsExistByKey(key string) bool {
	c := NewRedisPool().Get()
	defer c.Close()
	exist, err := redis.Bool(c.Do("EXISTS", key))
	if err != nil {
		fmt.Println(err)
		return false
	} else {
		return exist
	}
}


////////////////////////////
///       SETNX	         ///
////////////////////////////


func SetJson(key string, data interface{}) error {
	c := NewRedisPool().Get()
	defer c.Close()
	value, _ := json.Marshal(data)
	n, _ := c.Do("SETNX", key, value)
	if n != int64(1) {
		return errors.New("set failed")
	}
	return nil
}

func GetJsonByByte(key string) ([]byte, error) {
	c := NewRedisPool().Get()
	jsonGet, err := redis.Bytes(c.Do("GET", key))
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return jsonGet, nil
}

////////////////////////////
///       hex-table      ///
////////////////////////////

func HSet(table, key string, value interface{}) {
	c := NewRedisPool().Get()
	defer c.Close()
	_, err := c.Do("hset", table, key, value)
	if err != nil {
		fmt.Println("haset failed", err.Error())
	}
}

func HGet(table, key string) string {
	c := NewRedisPool().Get()
	defer c.Close()
	res, err := c.Do("hget", table, key)
	if err != nil {
		fmt.Println("hget failed", err.Error())
	}
	if res == nil {
		return ""
	}else {
		return string(res.([]byte))
	}
}

//var s struct {
//	Title  string `redis:"title"`
//	Author string `redis:"author"`
//	Body   string `redis:"body"`
//}
func HSetModel(s interface{}) {
	c := NewRedisPool().Get()
	defer c.Close()
	if _, err := c.Do("hmset", redis.Args{}.Add(reflect.TypeOf(s).Elem().Name()).AddFlat(s)...); err != nil {
		fmt.Println("HSetModel failed", err.Error())
	}
}

//獲取多個value
func HMGet(table string, keyAry []string) []string {
	returnString :=make([]string,0)
	c := NewRedisPool().Get()
	defer c.Close()

	for _, value := range keyAry {
		res, err := c.Do("hget", table, value)
		if err != nil {
			returnString = append(returnString,"key not found" )
		}else if res == nil {
			returnString = append(returnString,"" )
		}else {
			returnString = append(returnString, string(res.([]byte)) )
		}
	}
	return returnString
}


func HDelKey(table,key string) error {
	var err error
	c := NewRedisPool().Get()
	defer c.Close()
	if _, err = c.Do("hdel",table,key ); err != nil {
		fmt.Println("HSetModel failed", err.Error())
	}
	return  err
}



func IsExistByHexTableKey(table,key string) bool {
	c := NewRedisPool().Get()
	defer c.Close()
	exist, err := redis.Bool(c.Do("hexists", table, key))
	if err != nil {
		return false
	} else {
		return exist
	}
}