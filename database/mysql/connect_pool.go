package mysql

import (
	"database/sql"
	"file_server/config"
	"file_server/util/log"
)

type connectDB struct {
	connectDB *sql.DB
}

//Global Config Variable
var ConnectDB 		connectDB

func OpenConnect() {
	db, err := sql.Open("mysql",
		"" + config.DataBaseConfig.Username +
			":" + config.DataBaseConfig.Password +
			"@tcp(" + config.DataBaseConfig.Host +
			":" + config.DataBaseConfig.Port +
			")/" + config.DataBaseConfig.DBName + "?parseTime=true&loc=Local")

	if err != nil {
		log.Error(err)
	}

	err = db.Ping()
	if err!=nil{
		log.Error(err)
	}

	db.SetMaxOpenConns(200) //最大連線數 防止to many connect
	//db.SetMaxIdleConns(100) //閒置連線數  加快效能 但是會佔連線數

	ConnectDB.connectDB = db
}


func CloseConnect() {
	log.Error(ConnectDB.connectDB.Close())
}