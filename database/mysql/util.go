package mysql

import (
	_string "file_server/util/string"
)

func addPrefixTableName(tableName string, column string, tags []string) string {
	tn := ""
	if len(tags) > 0 {
		for _, v := range tags {
			if v == column {
				tn = "`"+ tableName + "`"+ "."
				break
			}
		}
	} else {
		tn = "`"+ tableName + "`"+ "."
	}

	return tn + _string.TrimQuotes(column)
}
