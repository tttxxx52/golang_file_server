package model

import "file_server/database/redis"

type Test struct {
	Title  string `redis:"title"`
	Author string `redis:"author"`
	Body   string `redis:"body"`
}


func (model *Test) Set(){
	model.Title = "title"
	model.Author = "Author"
	model.Body = "body"
	redis.HSetModel(model)
}