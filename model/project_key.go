package model

import (
	"file_server/database/mysql"
	"file_server/util/log"
	"fmt"
	"time"
)

type ProjectKey struct {
	Project   string    `table:"project"`
	ApiKey    string    `table:"api_key"`
	Secret	  string 	`table:"secret"`
	CreatedAt time.Time `table:"created_at"`
}

func (model *ProjectKey) SetProject(project string) *ProjectKey {
	model.Project = project
	return model
}

func (model *ProjectKey) SetApiKey(apiKey string) *ProjectKey {
	model.ApiKey = apiKey
	return model
}

func (model *ProjectKey) SetSecret(secret string) *ProjectKey {
	model.Secret = secret
	return model
}

func (model *ProjectKey) SetCreatedAt(createdAt time.Time) *ProjectKey {
	model.CreatedAt = createdAt
	return model
}

func (model *ProjectKey) QueryOne() *ProjectKey {
	log.Error(mysql.Model(model).
		Where("api_key", "like", model.ApiKey).
		Select([]string{"project"}).
		Find().
		Scan(&model.Project))
	return model
}

func (model *ProjectKey) Create() {
	_,err := mysql.Model(model).Insert()
	fmt.Println(err)
}