package model

import (
	"file_server/config"
	"file_server/util/file"
	"fmt"
	"strconv"
	"time"
)

const (
	//ImgFloderHeadPhoto  = "head/"
	//ImgFloderCommentPhoto  = "comment/"
)

// 產生檔案路徑並回傳
func GenerateFilePath(fileExtension string, transType string) (string, string, string, error) {

	// get unix time (nanosecond => microsecond)
	unixTime := time.Now().UnixNano() / int64(time.Microsecond)//跑迴圈用millisecond產生檔名時，會因為時間差太短而導致檔名一樣，故要用microsecond
	// int64 to string
	strTime := strconv.FormatInt(unixTime, 10)

	saveDir := fmt.Sprintf("%s//%s", config.PathConfig.SaveDirectoryRoot, transType)
	uploadDir := fmt.Sprintf("%s//%s", config.PathConfig.HttpDirectoryRoot, transType)

	if err := file.CreateDirectoryIfNotExist(saveDir); err != nil {
		return "", "", "", err
	}

	return fmt.Sprintf("%s%s%s", saveDir, strTime, fileExtension), fmt.Sprintf("%s%s%s", uploadDir, strTime, fileExtension), fmt.Sprintf("%s%s%s", transType, strTime, fileExtension), nil
}
