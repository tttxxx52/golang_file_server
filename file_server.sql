/*
 Navicat Premium Data Transfer

 Source Server         : tttxxx52
 Source Server Type    : MariaDB
 Source Server Version : 100143
 Source Host           : 107.167.185.29:3306
 Source Schema         : file_server

 Target Server Type    : MariaDB
 Target Server Version : 100143
 File Encoding         : 65001

 Date: 25/01/2020 15:34:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for project_key
-- ----------------------------
DROP TABLE IF EXISTS `project_key`;
CREATE TABLE `project_key` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project` varchar(255) NOT NULL,
  `api_key` varchar(20) NOT NULL,
  `secret` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

SET FOREIGN_KEY_CHECKS = 1;
