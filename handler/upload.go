package handler

import (
	"encoding/base64"
	"file_server/model"
	"file_server/util"
	"file_server/util/log"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/nfnt/resize"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"path"
	"path/filepath"
)

const (
	narrowPx = 1000
)

func Upload(c *gin.Context) {
	var (
		saveDirList []string
		floderPath  string
		projectKey  model.ProjectKey
	)
	key, _ := c.Get("projectKey")
	driveType, _ := c.Get("driveType")
	apiKey := key.(string)
	folder := c.PostForm("folder")

	//--------------------多檔接收----------------
	if projectKey.SetApiKey(apiKey).QueryOne().Project == "" {
		c.JSON(http.StatusOK, util.RS{Message: "Api Key 錯誤", Status: false})
		return
	} else if form, err := c.MultipartForm(); err == nil {
		var fromFile string
		if driveType == "flutter" {
			fromFile = "file[]"
		} else {
			fromFile = "file"
		}

		if fileList := form.File[fromFile]; fileList != nil {

			floderPath = projectKey.Project + folder + "/"

			//檢查是否為圖片檔
			for _, file := range fileList {
				fileExt := filepath.Ext(file.Filename)
				if !(fileExt == ".jpg" || fileExt == ".png" || fileExt == ".gif" || fileExt == ".jpeg") {
					c.JSON(http.StatusOK, util.RS{Message: "只能上傳圖片檔", Status: false})
					return
				}
			}

			for _, file := range fileList {
				//產生檔案路徑並判斷是否發生異常
				if saveDir, _, returnFileDir, err := model.GenerateFilePath(filepath.Ext(file.Filename), floderPath); saveDir == "" && err != nil {
					c.JSON(http.StatusOK, util.RS{Message: err.Error(), Status: false, Data: saveDirList})
					return
				} else {
					var img image.Image
					var narrowRate float64
					var narrowWidth, narrowHeight uint

					//將上傳的圖片用檔案形式打開
					src, err := file.Open()
					log.Error(err)
					defer src.Close()

					//依上傳的檔案轉為圖片型態
					switch filepath.Ext(file.Filename) {
					case ".jpg":
						img, err = jpeg.Decode(src)
						log.Error(err)
					case ".png":
						img, err = png.Decode(src)
						log.Error(err)
					case ".gif":
						img, err = gif.Decode(src)
						log.Error(err)
					case ".jpeg":
						img, err = jpeg.Decode(src)
						log.Error(err)
					}

					//取得圖片的高與寬
					b := img.Bounds()
					width := b.Max.X
					height := b.Max.Y

					//計算縮小比例
					if width > narrowPx || height > narrowPx {
						if width > height {
							narrowRate = math.Floor(float64(width / narrowPx))
						} else {
							narrowRate = math.Floor(float64(height / narrowPx))
						}

						//判斷比例是否接近縮小的尺寸，是的話則不用縮小
						if narrowRate == 0 {
							narrowRate = 1
						}

						narrowWidth = uint(width / int(narrowRate))
						narrowHeight = uint(height / int(narrowRate))
					} else {
						narrowWidth = uint(width)
						narrowHeight = uint(height)
					}

					//進行圖片尺寸壓縮
					zipImg := resize.Resize(narrowWidth, narrowHeight, img, resize.NearestNeighbor)

					//儲存檔案
					out, err := os.Create(saveDir)
					log.Error(err)
					defer out.Close()
					log.Error(err)

					//將壓縮的檔案編碼後輸出
					switch filepath.Ext(file.Filename) {
					case ".jpg":
						err = jpeg.Encode(out, zipImg, nil)
						log.Error(err)
					case ".png":
						err = png.Encode(out, zipImg)
						log.Error(err)
					case ".gif":
						err = gif.Encode(out, zipImg, nil)
						log.Error(err)
					case ".jpeg":
						err = jpeg.Encode(out, zipImg, nil)
						log.Error(err)
					}

					//儲存檔案
					//if err := c.SaveUploadedFile(file, saveDir); err != nil {
					//	c.JSON(http.StatusOK, util.RS{Message: err.Error(), Status: false, Data: saveDirList})
					//	return
					//}

					saveDirList = append(saveDirList, returnFileDir)
				}
			}

			c.JSON(http.StatusOK, util.RS{Message: "success", Status: true, Data: saveDirList})
		} else {
			c.JSON(http.StatusOK, util.RS{Message: "no file", Status: false})
		}
	} else {
		c.JSON(http.StatusOK, util.RS{Message: "bad form，error message:" + err.Error(), Status: false})
	}

	//---------------------多檔接收----------------
}

func UploadBase64(c *gin.Context) {
	fmt.Println("call base64")
	var (
		saveDirList []string
		floderPath  string
		projectKey  model.ProjectKey
	)
	key, _ := c.Get("projectKey")
	driveType, _ := c.Get("driveType")
	apiKey := key.(string)
	folder := c.PostForm("folder")

	//--------------------多檔接收----------------
	if projectKey.SetApiKey(apiKey).QueryOne().Project == "" {
		c.JSON(http.StatusOK, util.RS{Message: "Api Key 錯誤", Status: false})
		return
	} else if form, err := c.MultipartForm(); err == nil {
		var fromFile string
		if driveType == "flutter" {
			fromFile = "file[]"
		} else {
			fromFile = "file"
		}

		if fileList := form.File[fromFile]; fileList != nil {

			floderPath = projectKey.Project + folder + "/"

			//檢查是否為圖片檔
			for _, file := range fileList {
				fileExt := filepath.Ext(file.Filename)
				if !(fileExt == ".jpg" || fileExt == ".png" || fileExt == ".gif" || fileExt == ".jpeg") {
					c.JSON(http.StatusOK, util.RS{Message: "只能上傳圖片檔", Status: false})
					return
				}
			}

			for _, file := range fileList {
				//產生檔案路徑並判斷是否發生異常
				if saveDir, _, _, err := model.GenerateFilePath(filepath.Ext(file.Filename), floderPath); saveDir == "" && err != nil {
					c.JSON(http.StatusOK, util.RS{Message: err.Error(), Status: false, Data: saveDirList})
					return
				} else {
					var img image.Image
					var narrowRate float64
					var narrowWidth, narrowHeight uint

					//將上傳的圖片用檔案形式打開
					src, err := file.Open()
					log.Error(err)
					defer src.Close()

					//依上傳的檔案轉為圖片型態
					switch filepath.Ext(file.Filename) {
					case ".jpg":
						img, err = jpeg.Decode(src)
						log.Error(err)
					case ".png":
						img, err = png.Decode(src)
						log.Error(err)
					case ".gif":
						img, err = gif.Decode(src)
						log.Error(err)
					case ".jpeg":
						img, err = jpeg.Decode(src)
						log.Error(err)
					}

					//取得圖片的高與寬
					b := img.Bounds()
					width := b.Max.X
					height := b.Max.Y

					//計算縮小比例
					if width > narrowPx || height > narrowPx {
						if width > height {
							narrowRate = math.Floor(float64(width / narrowPx))
						} else {
							narrowRate = math.Floor(float64(height / narrowPx))
						}
						//判斷比例是否接近縮小的尺寸，是的話則不用縮小
						if narrowRate == 0 {
							narrowRate = 1
						}
						narrowWidth = uint(width / int(narrowRate))
						narrowHeight = uint(height / int(narrowRate))
					} else {
						narrowWidth = uint(width)
						narrowHeight = uint(height)
					}

					//進行圖片尺寸壓縮
					zipImg := resize.Resize(narrowWidth, narrowHeight, img, resize.NearestNeighbor)

					//儲存檔案
					out, err := os.Create(saveDir)
					log.Error(err)
					defer out.Close()
					log.Error(err)

					//將壓縮的檔案編碼後輸出
					switch filepath.Ext(file.Filename) {
					case ".jpg":
						err = jpeg.Encode(out, zipImg, nil)
						log.Error(err)
					case ".png":
						err = png.Encode(out, zipImg)
						log.Error(err)
					case ".gif":
						err = gif.Encode(out, zipImg, nil)
						log.Error(err)
					case ".jpeg":
						err = jpeg.Encode(out, zipImg, nil)
						log.Error(err)
					}
					base64Image, err := ImageToBase64(saveDir)
					if err != nil {
						fmt.Println(err)
					}
					// 刪除檔案
					log.Error(os.Remove(saveDir))

					saveDirList = append(saveDirList, base64Image)
				}
			}

			c.JSON(http.StatusOK, util.RS{Message: "success", Status: true, Data: saveDirList})
		} else {
			c.JSON(http.StatusOK, util.RS{Message: "no file", Status: false})
		}
	} else {
		c.JSON(http.StatusOK, util.RS{Message: "bad form，error message:" + err.Error(), Status: false})
	}

	//---------------------多檔接收----------------
}

func ImageToBase64(filepath string) (string, error) {
	const base64Table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	var coder = base64.NewEncoding(base64Table)
	file, err := os.Open(filepath)
	if err != nil {
		log.Error(err)
	}
	data, err := ioutil.ReadAll(file)
	if err != nil {
		log.Error(err)
	}
	fileSuffix := string([]byte(path.Ext(path.Base(filepath)))[1:4])
	return "data:image/" + fileSuffix + ";base64," + string([]byte(coder.EncodeToString(data))), nil
}
