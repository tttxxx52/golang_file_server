package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func TokenPrecede(c *gin.Context) {
	t := time.Now()                  //取得現在時間
	tt := t.Format("20060102150405") //時間格式設定
	c.JSON(http.StatusOK, tt)
}