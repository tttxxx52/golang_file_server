package service

import (
	"context"
	"file_server/database/mongo"
	"file_server/util"
	"file_server/util/crypto"
	"file_server/util/crypto/flutter"
	"file_server/util/log"
	"fmt"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const CsrfToken = "c20d19eb0fa930"

// 判斷是否 使用者登入的 middleWare
func AuthToken() gin.HandlerFunc {
	return func(c *gin.Context) {

		var projectKey string
		status := false
		driveType := c.GetHeader("drive-type")
		token := c.GetHeader("Auth-Token")

		if token != ""{
			// 解析 Token
			if driveType == "flutter"{
				projectKey, status = ParseTokenByFlutter(token)
			}else {
				driveType = "web"
				projectKey, status = ParseToken(token)
			}

		}

		//判斷token是否重複
		if isRepeatToken(token) {
			return
		}

		//新增token
		status = insertToken(token)
		if status {
			c.Set("projectKey", projectKey)
			c.Set("driveType",driveType)
			c.Next()
			return
		} else {
			// 未授權的身份
			c.AbortWithStatusJSON(http.StatusOK, util.RS{Message: "logout", Status: false})
			return
		}
	}
}

func insertToken(token string) bool {
	status := false
	mongo.NewMongo(func(handler *mongo.MongoHandler) {
		type Token struct {
			Token string
		}
		token := Token{token}
		_, err :=handler.Collection("token").InsertOne(context.Background(), token)
		if err != nil {
			status = false
		}else {
			status = true
		}
	})
	return  status
}

func isRepeatToken(token string) bool {
	isRepeat := true
	mongo.NewMongo(func(handler *mongo.MongoHandler) {
		cursor:=handler.Collection("token").FindOne(context.Background(),bson.M{"token":token})
		if err:=cursor.Err(); err!= nil {
			isRepeat = false
		}else {
			isRepeat = true
		}
	})

	return  isRepeat
}

func verifyCsrfToken(token string) bool {
	if token == "" {
		return false
	}

	tokenInfo, err := crypto.KeyDecrypt(token)
	if err != nil {
		log.Error(err)
		return false
	}

	return tokenInfo == CsrfToken
}

// go run main.go csrf:create
func GenerateCsrfToken() {
	token, err := crypto.KeyEncrypt(CsrfToken)
	log.Error(err)
	fmt.Print(token)
}

func GenerateToken(userId int64) (string, error) {
	temp := strconv.FormatInt(userId, 10) + ";" + time.Now().Format("20060102150405")
	token, err := crypto.KeyEncrypt(temp)
	if err != nil {
		return "", err
	}

	token, err = crypto.KeyEncrypt(token)
	if err != nil {
		return "", err
	}

	return token, nil
}

func ParseToken(token string) (string,  bool) {
	if token == "" {
		return "", false
	}


	tokenInfo, err := crypto.KeyDecrypt(token)
	if err != nil {
		log.Error(err)
		return "", false
	}

	spiltStr := strings.Split(tokenInfo, ";")

	if len(spiltStr) == 2 {
		projectKey := spiltStr[0]
		local, err := time.LoadLocation("Asia/Taipei")

		if err != nil {
			log.Error(err)
			return "", false
		}

		t, err := time.ParseInLocation("20060102150405", spiltStr[1], local)
		if err != nil {
			log.Error(err)
			return "", false
		}

		// 檢查 token 時效
		if util.TimeNow().Sub(t).Hours() >= 24 {
			return "", false
		}

		return projectKey, true
	} else {
		return "", false
	}
}


func ParseTokenByFlutter(token string) (string,  bool) {
	if token == "" {
		return "", false
	}


	tokenInfo, err := flutter.Decrypt(token)
	if err != nil {
		log.Error(err)
		return "", false
	}

	spiltStr := strings.Split(tokenInfo, ";")

	if len(spiltStr) == 2 {
		projectKey := spiltStr[0]
		local, err := time.LoadLocation("Asia/Taipei")

		if err != nil {
			log.Error(err)
			return "", false
		}

		t, err := time.ParseInLocation("20060102150405", spiltStr[1], local)
		if err != nil {
			log.Error(err)
			return "", false
		}

		// 檢查 token 時效
		if util.TimeNow().Sub(t).Hours() >= 24 {
			return "", false
		}

		return projectKey, true
	} else {
		return "", false
	}
}
